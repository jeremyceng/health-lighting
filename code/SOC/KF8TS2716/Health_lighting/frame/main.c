
#include <KF8TS2716.h>
#include "jxos_public.h"

void print_uart_init(void)
{
	BRCTL=0x40;
	EUBRGH=0x00;	//aud=8000000/(64*(12+1))=9615
	EUBRGL=0x0C;
	TSCTL=0x22;
	RSCTL=0x90;
	TXIF=0;
	TXIE=0;
	RCIF=0;
	RCIE=1;
	PRC = 0;	//0: low level interrupt
	TR21 = 0;	//TX pin
}

void mcu_init()
{
	OSCCTL = 0x60;//8M Hz

	TR0 = 0XFF;
	TR1 = 0xFF;
	TR2 = 0XFF;
	TR3 = 0XFF;

    P0LR=0xFF;
    P1LR=0xFF;
    P2LR=0xFF;
    P3LR=0XFF;

    P0=0xFF;
    P1=0xFF;
    P2=0xFF;
    P3=0xFF;

	//pwm_timer_init
	//init_T1
	T1CTL = 0x10;  // 8M/4 -> 1/2 -> 1M   -> 1 tick = 1us
//	T1H = 0XEC;
//	T1L = 0X78;	   //(0XFFFF+1) - 5000us -> 5ms
	T1H = 0XFF;
	T1L = 0X9c;	   //(0XFFFF+1) - 100us -> 100us
	T1IF = 0;
	T1IE = 1;
	T1ON = 1;
	PT1 = 1; 	//1: high level interrupt
	
	print_uart_init();

/**********************************/
	// TR37 = 0;	//IR_EN
	// TR12 = 1;	//IR_REC	
}

void mcu_run(void)
{
	IPEN = 1;
	AIEH = 1;
	AIEL = 1;
}


void delay_us(uint16_t us_data)
{
	while(us_data--);
}

//void delay_ms(uint16_t ms_data)
//{
//	uint16_t j=0;
//	while(ms_data--)
//	{
//		j=200;
//		_CWDT();
//		while(j--)
//		{
//			_CWDT();
//		}
//	}
//}
//
//void Delay_ms(uint16_t ms_data)
//{
//	uint8_t i;
//	while(ms_data--)
//	{
//		i = 124;
//		while(i--);
//	}
//}

void delay_595(void)
{
	delay_us(50);
}

void main()
{
	jxos_run();
}

void _my_fputc(char ch)
{TXSDR=ch; while(TXSRS==0);}

__sfr  __at(0x2007) CONFIG =0x05c4;
