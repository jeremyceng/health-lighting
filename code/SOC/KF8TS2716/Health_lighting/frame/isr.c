
#include <KF8TS2716.h>
#include "jxos_public.h"

#include "fan.h"
#include "light.h"
#include "led.h"

/**************************************/
#define period	100

static volatile uint8_t pwm_counter;

static volatile uint8_t pwm_c_light_duty;
static volatile uint8_t pwm_w_light_duty;
static volatile uint8_t pwm_led_r_duty;
static volatile uint8_t pwm_led_g_duty;
static volatile uint8_t pwm_led_b_duty;

/**************************************/
void pwm_fan_init(void)
{
	fan_off();
}
void pwm_fan_set(uint8_t duty)
{
    if(duty == 0){
    	fan_off();
    }
	else{
		fan_on();
	}
}

/**************************************/
void pwm_light_init(void)
{
	pwm_c_light_duty = 0;
	c_light_off();
	pwm_w_light_duty = 0;
	w_light_off();
}
void pwm_c_light_set(uint8_t duty)
{
	pwm_c_light_duty = duty;
    if(duty == 0){
    	c_light_off();
    }
}
void pwm_w_light_set(uint8_t duty)
{
	pwm_w_light_duty = duty;
    if(duty == 0){
    	w_light_off();
    }
}

/**************************************/
void pwm_led_init(void)
{
	pwm_led_r_duty = 0;
	led_r_off();
	pwm_led_g_duty = 0;
	led_g_off();
	pwm_led_b_duty = 0;
	led_b_on();
}
void pwm_led_r_set(uint8_t duty)
{
	pwm_led_r_duty = duty;
    if(duty == 0){
    	led_r_off();
    }
}
void pwm_led_g_set(uint8_t duty)
{
	pwm_led_g_duty = duty;
    if(duty == 0){
    	led_g_off();
    }
}
void pwm_led_b_set(uint8_t duty)
{
	pwm_led_b_duty = duty;
    if(duty == 0){
    	led_b_off();
    }
}

/**************************************/
void int_fun1() __interrupt (0) //(0) -> 0X04 high level
{
	if(T1IF)
	{
 		T1IF = 0;
		T1H = 0XFF;
		//T1L = 0X9C;	   //100us 9c
		T1L = 0XCE;			//50us
 		/**************************************/
		pwm_counter++;
		if(pwm_counter >= period){
			pwm_counter = 0;
			if(pwm_c_light_duty> 0){
				c_light_on();
			}
			if(pwm_w_light_duty> 0){
				w_light_on();
			}
			if(pwm_led_r_duty> 0){
				led_r_on();
			}
			if(pwm_led_g_duty> 0){
				led_g_on();
			}
			if(pwm_led_b_duty> 0){
				led_b_on();
			}
		}
		if(pwm_counter >= pwm_c_light_duty){
			c_light_off();
		}
		if(pwm_counter >= pwm_w_light_duty){
			w_light_off();
		}
		if(pwm_counter >= pwm_led_r_duty){
			led_r_off();
		}
		if(pwm_counter >= pwm_led_g_duty){
			led_g_off();
		}
		if(pwm_counter >= pwm_led_b_duty){
			led_b_off();
		}
 		/**************************************/
	}

	if(T0IF)
	{
		T0IF = 0;
		T0 = 256 - 77;      // 78*128us = 9984us ~~ 10ms
		software_timer_task_tick_handler();
	}
}

/**************************************/
#define REC_STATE_WAIT_HEAD_1	0
#define REC_STATE_WAIT_HEAD_2	1
#define REC_STATE_REC_DATA		2
#define REC_STATE_REC_CHECKSUM	4

/**************************************/
uint8_t uart_frame_buffer[32] = {0}; // Frame header : 0x42,0x4d
uint8_t uart_rec_finish_flag;
static volatile uint8_t uart_rec_state;
static volatile uint8_t uart_rec_count;
static volatile uint8_t uart_clr_rec;
static volatile uint16_t checksum;
static volatile uint16_t temp;

void uart_rec_init(void)
{
	uart_rec_finish_flag = 0;
	uart_rec_state = REC_STATE_WAIT_HEAD_1;
	uart_rec_count = 0;
	uart_clr_rec = 0;
	checksum = 0;
	temp = 0;
}

/**************************************/
void int_fun() __interrupt (1)	//(1) -> 0X14 low level
{
	if(RCIF == 1){
//		RCIF = 0;

		if(uart_rec_finish_flag == 1){
			uart_clr_rec = RXSDR;
			return;
		}

		uart_frame_buffer[uart_rec_count] = RXSDR;
		if(uart_rec_state == REC_STATE_WAIT_HEAD_1){
			//if(RXSDR == 0x42){
			if(RXSDR == 0x32){
				uart_rec_count++;
				uart_rec_state = REC_STATE_WAIT_HEAD_2;
			}
		}
		else if(uart_rec_state == REC_STATE_WAIT_HEAD_2){
			//if(RXSDR == 0x4d){
			if(RXSDR == 0x3d){
				uart_rec_count++;
				uart_rec_state = REC_STATE_REC_DATA;
			}
			else{
				uart_rec_count = 0;
				uart_rec_state = REC_STATE_WAIT_HEAD_1;
			}
		}
		else if (uart_rec_state == REC_STATE_REC_DATA) {
			if (uart_rec_count == 31) {
				checksum = 0;
				for (temp = 0; temp < 30; temp++) {
					checksum += uart_frame_buffer[temp];
				}

				temp = uart_frame_buffer[30];
				temp <<= 8;
				temp += uart_frame_buffer[31];

				//checksum is right
				if (checksum == temp) {
					uart_rec_finish_flag = 1;
				}
				uart_rec_count = 0;
				uart_rec_state = REC_STATE_WAIT_HEAD_1;
			}
			else {
				uart_rec_count++;
			}
		}
	}
}
