
#include <KF8TS2716.h>
#include "jxos_public.h"
#include "led.h"
#include "digital_display.h"

void sys_software_timer_task_hal_init_callback_handler(void)
{
	//init_T1
//	T1CTL = 0x10;  // 8M/4 -> 1/2 -> 1M   -> 1 tick = 1us
//	T1H = 0XEC;
//	T1L = 0X78;	   //(0XFFFF+1) - 5000us -> 5ms
//	T1IF = 0;      //���T1�жϱ�־
// 	T1IE = 1;	   //T1�ж�ʹ��
// 	PUIE = 1;      //ʹ�������ж�

	//init_T0
//	PT0 = 0;
//	OPTR =0x02;// set time0 and  frequency divider 2 is 1:8  set 1ms 1k,T0 clk is fosc/4   -> 1 tick = 4us
//	T0 = 256 - 250;      //��ʱ1ms 250*4us = 1ms
// 	T0IF = 0;      //���T0�жϱ�־
// 	T0IE = 1;	   //T0�ж�ʹ��

	OPTR =0x07; //(8m/4)/256hz  -> 1 tick = 128us
	T0 = 256 - 78;      // 78*128us = 9984us ~~ 10ms
 	T0IF = 0;
 	T0IE = 1;
 	PT0 = 1;	//1: high level interrupt
}

void sys_software_timer_task_hal_start_callback_handler(void)
{
	T0CS = 0;
 	T0IE = 1;
}

void sys_software_timer_task_hal_stop_callback_handler(void)
{
	T0CS = 1;
 	T0IE = 0;
}



void button_hal_init_config_handler(void)
{
	P15 = 1;
	P16 = 1;
	P17 = 1;

	PUR15 = 1;
	PUR16 = 1;
	PUR17 = 1;

	TR15 = 1;
	TR16 = 1;
	TR17 = 1;
}

bool_t button_read_pin_level_config_handler(uint8_t button_num)
{
	bool_t button_level = 0;
	switch (button_num)
	{
	case 0:
		button_level = P15;
		break;

	case 1:
		button_level = P16;
		break;

	case 2:
		button_level = P17;
		break;

	default:
		break;
	}

	return button_level;
}

void value_move_hal_init_config_handler(void)
{
}

extern uint16_t target_color_temperature;
extern uint8_t target_light_level;
void value_move_set_value_event_handler(uint8_t pwm_move_num, uint16_t duty)
{
	switch (pwm_move_num)
	{
	case 0:
		pwm_led_r_set((uint8_t)duty);
		break;

	case 1:
		pwm_led_g_set((uint8_t)duty);
		break;

	case 2:
		pwm_led_b_set((uint8_t)duty);
		break;

	case 3:
		target_light_level = (uint8_t)duty;
		break;
		
	case 4:
		target_color_temperature = duty;
		break;

	case 5:
		digital_display_set_lum((uint8_t)duty);
		break;

	default:
		break;
	}
}
