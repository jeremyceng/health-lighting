


void value_move_hal_init_config_handler(void);
void value_move_set_value_event_handler(uint8_t pwm_move_num, uint16_t duty);

#define VALUE_MOVE_NUM_MAX 		6

#define value_move_hal_init_config()                            value_move_hal_init_config_handler()
#define value_move_set_value_event(value_move_num, set_value)   value_move_set_value_event_handler(value_move_num, set_value)

