


#include <jxos_config.h>

void button_hal_init_config_handler(void);
bool_t button_read_pin_level_config_handler(uint8_t button_num);

void standard_app_button_scan_task_button_press_event_handler(uint8_t button_num);
void standard_app_button_scan_task_button_release_event_handler(uint8_t button_num);
void standard_app_button_scan_task_button_long_press_event_handler(uint8_t button_num);
void standard_app_button_scan_task_button_long_press_repeat_event_handler(uint8_t button_num);

#define BUTTON_NUM_MAX 		                BUTTON_SCAN_TASK_BUTTON_NUM_MAX
#define BUTTON_PRESS_LEVEL				    BUTTON_SCAN_TASK_BUTTON_PRESS_LEVEL

#define BUTTON_JITTER_TICK					0
#define BUTTON_LONG_PRESS_TICK 		        BUTTON_SCAN_TASK_BUTTON_LONG_PRESS_TIME_MS/BUTTON_SCAN_TASK_SCAN_TICK_TIME_MS
#define BUTTON_LONG_PRESS_REPEAT_TICK 		BUTTON_SCAN_TASK_BUTTON_LONG_PRESS_REPEAT_TIME_MS/BUTTON_SCAN_TASK_SCAN_TICK_TIME_MS

#define button_hal_init_config()                    button_hal_init_config_handler()
#define button_read_pin_level_config(button_num)    button_read_pin_level_config_handler(button_num)

#define button_free_event(button_num)               
#define button_press_event(button_num)              standard_app_button_scan_task_button_press_event_handler(button_num)
#define button_release_event(button_num)            standard_app_button_scan_task_button_release_event_handler(button_num)
#define button_long_press_event(button_num)         standard_app_button_scan_task_button_long_press_event_handler(button_num)
#define button_long_press_repeat_event(button_num)  standard_app_button_scan_task_button_long_press_repeat_event_handler(button_num)