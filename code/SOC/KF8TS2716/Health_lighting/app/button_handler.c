

#include "jxos_public.h"
#include "state.h"

void button_handler_task(void)
{
	uint8_t button_i;
	for(button_i = 0; button_i < BUTTON_SCAN_TASK_BUTTON_NUM_MAX; button_i++){
		if(standard_app_button_event_msg[button_i] != 0xff){

    		if(standard_app_button_event_msg[button_i] == STD_APP_BUTTON_SCAN_EVENT_RELEASE){
    			if(button_i == 0){
					printf_string("cct release\r\n");
					state_cct_buttont_release_event_handler();
    			}
    			else if(button_i == 1){
					printf_string("onoff release\r\n");
    				state_onoff_buttont_release_event_handler();
    			}
    			else if(button_i == 2){
    				printf_string("level release\r\n");
    				state_level_buttont_release_event_handler();
    			}
    		}

    		if(standard_app_button_event_msg[button_i] == STD_APP_BUTTON_SCAN_EVENT_LONG_PRESS){
    			if(button_i == 0){
					printf_string("cct long\r\n");
					state_cct_buttont_long_press_event_handler();
    			}
    			else if(button_i == 1){
    				printf_string("onoff long\r\n");
    				state_onoff_buttont_long_press_event_handler();
    			}
    			else if(button_i == 2){
    				printf_string("level long\r\n");
    				state_level_buttont_long_press_event_handler();
    			}
    		}

			standard_app_button_event_msg[button_i] = 0xff;
		}
	}
}


void button_handler_task_init(void)
{
}
