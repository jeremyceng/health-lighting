
#include "lib/type.h"
#include "attribute_handler.h"
#include "digital_display.h"
#include "attribute_back.h"

#define STATE_ON        			0
#define STATE_OFF       			1
#define STATE_SLEEP     			2
#define STATE_SET_SLEEP_MODE	    3

#define MODE_SET_LIGHT     		0
#define MODE_SET_DELAY     		1
#define MODE_SET_FAN       		2

uint8_t set_light_level;
uint8_t set_fan_level;
uint8_t set_sleep_delay;

volatile uint8_t current_state;
volatile bool_t onoff_buttont_long_press_flag;
volatile bool_t cct_buttont_long_press_flag;
volatile bool_t level_buttont_long_press_flag;

uint16_t set_ct[] = {6500, 5000, 4000, 3000, 2700};
volatile uint8_t set_ct_p;
uint16_t set_level[] = {0,10,20,30,40,50,60,70,80,90,100};
volatile uint16_t set_level_p;

volatile uint8_t current_mode;
uint16_t set_light_mode[] = {0,5,30,50};
volatile uint16_t set_light_mode_p;
uint16_t set_delay_mode[] = {1,30,60,0};
volatile uint16_t set_delay_mode_p;
uint16_t set_fan_mode[] = {100,80,60,25};
volatile uint16_t set_fan_mode_p;

volatile uint16_t sleep_delay_counter;

void state_init(void)
{
    attribute_recover();

	if(set_light_level > 100){
		set_light_level = 100;
	}
	if(set_fan_level > 100){
		set_fan_level = 100;
	}
	if(set_sleep_delay > 60){
		set_sleep_delay = 1;
	}

    current_state = STATE_ON;
	cct_buttont_long_press_flag = false;
	onoff_buttont_long_press_flag = false;
	level_buttont_long_press_flag = false;

	set_ct_p = 0;
	set_level_p = 0;

	set_light_mode_p = 0;
	set_delay_mode_p = 0;
	set_fan_mode_p = 0;
}

void set_mode_handler(void)
{
	led_set_color(0,0,0);

	switch (current_mode){	
	case MODE_SET_LIGHT:
		digital_display_single(2, 'A');
		digital_display_single(0, 10);
		if(set_light_mode_p == 0)
		{
			digital_display_single(1, 1);
		}
		else if(set_light_mode_p == 1)
		{
			digital_display_single(1, 2);
		}
		else if(set_light_mode_p == 2)
		{
			digital_display_single(1, 3);
		}
		else if(set_light_mode_p == 3)
		{
			digital_display_single(1, 4);
		}
		else
		{
			set_light_mode_p = 0;
		}
		set_light_level = set_light_mode[set_light_mode_p];
		attribute_set_light_level(set_light_level);
	break;

	case MODE_SET_DELAY:
		if(set_delay_mode_p == 0)
		{
			digital_display_single(2, 0);
			digital_display_single(1, 1);
			digital_display_single(0, 10);
		}
		else if(set_delay_mode_p == 1)
		{
			digital_display_single(2, 3);
			digital_display_single(1, 0);
			digital_display_single(0, 10);
		}
		else if(set_delay_mode_p == 2)
		{
			digital_display_single(2, 6);
			digital_display_single(1, 0);
			digital_display_single(0, 10);
		}
		else if(set_delay_mode_p == 3)
		{
			digital_display_single(2, 0);
			digital_display_single(1, 'H');
			digital_display_single(0, 10);
		}
		else
		{
			set_delay_mode_p = 0;
		}
		set_sleep_delay = set_delay_mode[set_delay_mode_p];
	break;

	case MODE_SET_FAN:
		digital_display_single(2, 'F');
		digital_display_single(0, 10);
		if(set_fan_mode_p == 0)
		{
			digital_display_single(1, 1);
		}
		else if(set_fan_mode_p == 1)
		{
			digital_display_single(1, 2);
		}
		else if(set_fan_mode_p == 2)
		{
			digital_display_single(1, 3);
		}
		else if(set_fan_mode_p == 3)
		{
			digital_display_single(1, 4);
		}
		else
		{
			set_fan_mode_p = 0;
		}
		set_fan_level = set_fan_mode[set_fan_mode_p];
		attribute_set_fan_level(set_fan_level);
	break;		
	}
}

void state_onoff_buttont_release_event_handler(void)
{
	switch (current_state){
	case STATE_ON:
		if(onoff_buttont_long_press_flag == false){	//short
			current_state = STATE_SLEEP;
			attribute_save_current_attributer();
			display_trun_off();
			led_set_color(0,0,0);
			attribute_set_light_level(set_light_level);
			attribute_set_fan_level(set_fan_level);
			if(set_sleep_delay != 0){
				sleep_delay_counter = set_sleep_delay*60;
			}
			else{
				sleep_delay_counter = 0xffff;
			}
		}
		break;

	case STATE_OFF:
		if(onoff_buttont_long_press_flag == false){
			current_state = STATE_ON;
			display_trun_on();
			led_set_color(5,5,5);
			attribute_set_fan_level(100);
			attribute_recover_current_attributer();
		}
		break;

	case STATE_SLEEP:
		if(onoff_buttont_long_press_flag == false){
			current_state = STATE_ON;
			display_trun_on();
			led_set_color(5,5,5);
			attribute_set_fan_level(100);
			attribute_recover_current_attributer();
		}
		break;

	case STATE_SET_SLEEP_MODE:
		if(onoff_buttont_long_press_flag == false){
			current_state = STATE_ON;
			attribute_store();
			digital_display_show(0);
			led_set_color(5,5,5);
			attribute_set_fan_level(100);
			attribute_recover_current_attributer();
		}
		break;

	default:
		break;
	}

	onoff_buttont_long_press_flag = false;
}

void state_onoff_buttont_long_press_event_handler(void)
{
	switch (current_state){
	case STATE_ON:
		current_state = STATE_OFF;
		attribute_save_current_attributer();
		display_trun_off();
		led_set_color(0,0,0);
		attribute_set_fan_level(0);
		attribute_set_light_level(0);
		break;

	default:
		break;
	}

	onoff_buttont_long_press_flag = true;
}


void state_cct_buttont_release_event_handler(void)
{
	switch (current_state){
	case STATE_ON:
		if(cct_buttont_long_press_flag == false){
			attribute_set_color_temperature(set_ct[set_ct_p]);
			attribute_save_color_temperature(set_ct[set_ct_p]);
			set_ct_p++;
			if(set_ct_p > 4){
				set_ct_p = 0;
			}
		}
		else{
			attribute_color_temperature_stop_move();
			attribute_save_current_attributer();
		}

		break;

	case STATE_SET_SLEEP_MODE:
		if(cct_buttont_long_press_flag == false){
			if(current_mode == MODE_SET_LIGHT)
			{
				set_light_mode_p++;
				if(set_light_mode_p > 3)
				{
					set_light_mode_p = 0;
				}
			}
			if(current_mode == MODE_SET_DELAY)
			{
				set_delay_mode_p++;
				if(set_delay_mode_p > 3)
				{
					set_delay_mode_p = 0;
				}
			}
			if(current_mode == MODE_SET_FAN)
			{
				set_fan_mode_p++;
				if(set_fan_mode_p > 3)
				{
					set_fan_mode_p = 0;
				}
			}
			set_mode_handler();
		}
	break;

	default:
		break;
	}

	cct_buttont_long_press_flag = false;
}

void state_cct_buttont_long_press_event_handler(void)
{
	switch (current_state){
	case STATE_ON:
		if(level_buttont_long_press_flag == false)
		{
			attribute_move_color_temperature(1);
		}
		else
		{
			current_state = STATE_SET_SLEEP_MODE;
			attribute_color_temperature_stop_move();
			attribute_light_level_stop_move();
			attribute_save_current_attributer();
			current_mode = MODE_SET_LIGHT;
			set_mode_handler();
		}
		break;

	default:
		break;
	}

	cct_buttont_long_press_flag = true;
}


void state_level_buttont_release_event_handler(void)
{
	switch (current_state){
	case STATE_ON:
		if(level_buttont_long_press_flag == false){
			attribute_set_light_level(set_level[set_level_p]);
			attribute_save_light_level(set_level[set_level_p]);
			set_level_p++;
			if(set_level_p > 10){
				set_level_p = 0;
			}
		}
		else{
			attribute_light_level_stop_move();
			attribute_save_current_attributer();
		}
		break;

	case STATE_SET_SLEEP_MODE:
		if(level_buttont_long_press_flag == false){
			current_mode++;
			if(current_mode > MODE_SET_FAN){
				current_mode = MODE_SET_LIGHT;
			}
			set_mode_handler();
		}
		break;

	default:
		break;
	}
	
	level_buttont_long_press_flag = false;
}

void state_level_buttont_long_press_event_handler(void)
{
	switch (current_state){
	case STATE_ON:
		if(cct_buttont_long_press_flag == false)
		{
			attribute_move_light_level(1);
		}
		else
		{
			current_state = STATE_SET_SLEEP_MODE;
			attribute_color_temperature_stop_move();
			attribute_light_level_stop_move();
			attribute_save_current_attributer();
			current_mode = MODE_SET_LIGHT;
			set_mode_handler();
		}
		break;

	default:
		break;
	}

	level_buttont_long_press_flag = true;
}


void state_pm25_data_rec_event_handler(volatile uint16_t pm25)
{
//	printf_16bit_hex(pm25);
//	printf_string(" rec pm2.5\r\n");
	if(current_state == STATE_ON){
		if(pm25 < 12){
			led_set_color(0, 8, 0);
			led_set_speed(110);
		}
		else if(pm25 < 35){
			led_set_color(8, 8, 0);
			led_set_speed(90);
		}
		else if(pm25 < 55){
			led_set_color(10, 5, 0);
			led_set_speed(70);
		}
		else if(pm25 < 150){
			led_set_color(10, 0, 0);
			led_set_speed(50);
		}
		else if(pm25 < 250){
			led_set_color(8, 0, 8);
			led_set_speed(30);
		}
		else {
			led_set_color(5, 0, 10);
			led_set_speed(10);
		}
		digital_display_show(pm25);
	}
}

void state_second_tick_event_handler(void)
{
	if(current_state == STATE_SLEEP){
		if(sleep_delay_counter != 0xffff){
			sleep_delay_counter--;
			if(sleep_delay_counter == 0){
				current_state = STATE_OFF;
				display_trun_off();
				led_set_color(0,0,0);
				attribute_set_fan_level(0);
				attribute_set_light_level(0);
			}
		}
	}
}
