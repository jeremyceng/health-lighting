

#ifndef __STATE_H
#define __STATE_H

#include "lib/type.h"

void state_init(void);

void state_onoff_buttont_release_event_handler(void);
void state_onoff_buttont_long_press_event_handler(void);

void state_cct_buttont_release_event_handler(void);
void state_cct_buttont_long_press_event_handler(void);

void state_level_buttont_release_event_handler(void);
void state_level_buttont_long_press_event_handler(void);

void state_pm25_data_rec_event_handler(uint16_t pm25);

void state_second_tick_event_handler(void);

#endif
