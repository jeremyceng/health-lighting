
#ifndef __LIGHT_H
#define __LIGHT_H

#include <KF8TS2716.h>
#include "lib/type.h"

#define light_init()		P2LR7 = 1;P2LR6 = 1;TR27 = 0;TR26 = 0

#define c_light_on()		P2LR7 = 1
#define c_light_off()		P2LR7 = 0

#define w_light_on()		P2LR6 = 1
#define w_light_off()		P2LR6 = 0

void pwm_light_init(void);
void pwm_c_light_set(uint8_t duty);
void pwm_w_light_set(uint8_t duty);

#endif
