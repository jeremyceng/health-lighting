
#ifndef __FAN_H
#define __FAN_H

#include <KF8TS2716.h>
#include "lib/type.h"

#define fan_init()		P1LR0 = 0;TR10 = 0
#define fan_on()		P1LR0 = 1
#define fan_off()		P1LR0 = 0

void pwm_fan_init(void);
void pwm_fan_set(uint8_t duty);

#endif
