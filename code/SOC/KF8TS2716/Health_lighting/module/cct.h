

#ifndef __CCT_H
#define __CCT_H


#include "lib/type.h"

bool_t cct_get_duty(uint16_t color_temperature, uint8_t* cold_duty, uint8_t* warm_duty);

#endif

