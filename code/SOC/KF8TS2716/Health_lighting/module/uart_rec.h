
#ifndef __UART_REC_H
#define __UART_REC_H

#include "lib/type.h"

extern uint8_t uart_frame_buffer[];
extern uint8_t uart_rec_finish_flag;
void uart_rec_init(void);

#endif