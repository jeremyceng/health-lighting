

#include "lib/type.h"

const uint8_t cctTable[][2]={
{0, 100},
{3, 95},
{7, 91},
{11, 87},
{15, 83},
{20, 80},
{23, 75},
{27, 71},
{31, 67},
{36, 62},
{38, 60},
{42, 56},
{47, 51},
{49, 49},
{52, 46},
{55, 43},
{57, 41},
{60, 38},
{63, 35},
{65, 33},
{68, 30},
{71, 27},
{73, 25},
{76, 22},
{78, 20},
{81, 17},
{83, 15},
{84, 14},
{87, 11},
{89, 9},
{90, 8},
{91, 7},
{92, 6},
{93, 5},
{94, 4},
{95, 3},
{96, 2},
{97, 1},
{100, 0},
};

#define CCT_MIN     2700
#define CCT_MAX     6500
#define CCT_STEP    100

#define C_OFFSET    0
#define W_OFFSET    1

static uint8_t __ratio(uint16_t a_reference,
					   uint16_t a_start, uint16_t a_end,
					   uint8_t b_start, uint8_t b_end)
{
	uint16_t b_reference;

    if(a_reference == a_start){
        return b_start;
    }
    if(a_reference == a_end){
        return b_end;
    }

	//(a_reference-a_start)/(a_end-a_start) = (b_reference-b_start)/(b_end-b_start)
	//b_reference = ((a_reference-a_start)*(b_end-b_start))/(a_end-a_start)+b_start
//	printf("\r\n------->%d, %d, %d, %x, %x\r\n", a_reference, a_start, a_end, b_start, b_end);
	if(a_start >= a_end){
		a_reference = (a_reference-a_start);
		a_start = (a_start-a_end);
	}
	else{
		a_reference = (a_reference-a_start);
		a_start = (a_end-a_start);
	}

	if(b_start >= b_end){
		b_reference = b_start-(((b_start-b_end)*a_reference)/a_start);
	}
	else{
		b_reference = (((b_end-b_start)*a_reference)/a_start)+b_start;
	}
//	printf("b_reference %x\r\n",b_reference);
	return (uint8_t)b_reference;
}

bool_t cct_get_duty(uint16_t color_temperature, uint8_t* cold_duty, uint8_t* warm_duty)
{
    uint8_t i;
    uint16_t color_temperature_start;
    uint16_t color_temperature_end;

    if((color_temperature < CCT_MIN)||(color_temperature > CCT_MAX)){
        return false;
    }
    if(color_temperature == CCT_MAX){
        *cold_duty = cctTable[(CCT_MAX-CCT_MIN)/CCT_STEP][C_OFFSET];
        *warm_duty = cctTable[(CCT_MAX-CCT_MIN)/CCT_STEP][W_OFFSET];
        return true;
    }
    i = (color_temperature-CCT_MIN)/100;

    color_temperature_start = i;
    color_temperature_start = color_temperature_start*100 + CCT_MIN;

    color_temperature_end = i+1;
    color_temperature_end = color_temperature_end*100 + CCT_MIN;

    *cold_duty = __ratio(color_temperature,
                    color_temperature_start,
                    color_temperature_end,
                    cctTable[i][C_OFFSET],
                    cctTable[i+1][C_OFFSET]);
    *warm_duty = __ratio(color_temperature,
                    color_temperature_start,
                    color_temperature_end,
                    cctTable[i][W_OFFSET],
                    cctTable[i+1][W_OFFSET]);

    return true;
}

