
#include "flash.h"
#include "lib/type.h"

#define ATTRIBUTE_STORAGE_BLOCK_ADDR		0x1fc0

// extern uint16_t last_color_temperature;
// extern uint8_t last_light_level;
// uint8_t last_fan_level = 0;
 
extern uint8_t set_light_level;
extern uint8_t set_fan_level;
extern uint8_t set_sleep_delay;

void attribute_store(void)
{
	uint16_t temp;

	// /**************************************/
	// FLASH_BUFFER[0] = last_color_temperature;

	// /**************************************/
	// temp = last_light_level;
	// temp <<= 8;
	// temp += last_fan_level;
	// FLASH_BUFFER[1] = temp;

	/**************************************/
	temp = set_light_level;
	temp <<= 8;
	temp += set_fan_level;
	FLASH_BUFFER[2] = temp;

	/**************************************/
	temp = set_sleep_delay;
	temp <<= 8;
	temp += set_sleep_delay;
	FLASH_BUFFER[3] = temp;

	/**************************************/
	FLASH_WRITE_FUN(ATTRIBUTE_STORAGE_BLOCK_ADDR, FLASH_BUFFER_MAX);
}

void attribute_recover(void)
{
	uint16_t temp;

	//FLASH_READ_FUN(ATTRIBUTE_STORAGE_BLOCK_ADDR, FLASH_BUFFER_MAX);
	// temp = FLASH_BUFFER[0];

	///**************************************/	
	// last_color_temperature = FLASH_READ_ONE(ATTRIBUTE_STORAGE_BLOCK_ADDR);

	// /**************************************/	
	// temp = FLASH_READ_ONE(ATTRIBUTE_STORAGE_BLOCK_ADDR+1);
	// last_fan_level = (uint8_t)(temp&0x00ff);
	// temp >>= 8;
	// last_light_level = (uint8_t)temp;

	/**************************************/	
	temp = FLASH_READ_ONE(ATTRIBUTE_STORAGE_BLOCK_ADDR+2);
	set_fan_level = (uint8_t)(temp&0x00ff);
	temp >>= 8;
	set_light_level = (uint8_t)temp;

	/**************************************/	
	temp = FLASH_READ_ONE(ATTRIBUTE_STORAGE_BLOCK_ADDR+3);
	set_sleep_delay = (uint8_t)(temp&0x00ff);
	temp >>= 8;
	set_sleep_delay = (uint8_t)temp;
}

/**********************************
#include "lib/printf/_my_printf.h"
void attribute_test(void)
{
	last_color_temperature = 0x1234;
	last_light_level = 0xab;
	last_fan_level = 0xcd;
	set_light_level = 0x01;
	set_fan_level = 0x02;
	set_sleep_delay = 0x03;
	attribute_store();

	last_color_temperature = 0;
	last_light_level = 0;
	last_fan_level = 0;
	set_light_level = 0;
	set_fan_level = 0;
	set_sleep_delay = 0;
	printf_16bit_hex(last_color_temperature);
	printf_string(" ");
	printf_byte_hex(last_light_level);
	printf_string(" ");
	printf_byte_hex(last_fan_level);
	printf_string(" ");
	printf_byte_hex(set_light_level);
	printf_string(" ");
	printf_byte_hex(set_fan_level);
	printf_string(" ");
	printf_byte_hex(set_sleep_delay);
	printf_string(" ");

	printf_string("------------------\r\n");
	attribute_recover();
	printf_16bit_hex(last_color_temperature);
	printf_string(" ");
	printf_byte_hex(last_light_level);
	printf_string(" ");
	printf_byte_hex(last_fan_level);
	printf_string(" ");
	printf_byte_hex(set_light_level);
	printf_string(" ");
	printf_byte_hex(set_fan_level);
	printf_string(" ");
	printf_byte_hex(set_sleep_delay);
	printf_string(" ");
}
***********************************/
