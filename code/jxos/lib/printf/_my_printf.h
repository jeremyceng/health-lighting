
#ifndef _MY_PRINTF_H_
#define _MY_PRINTF_H_

#include "lib/type.h"

void _my_fputc(char ch);

void printf_putc_callback_regist(int (*putc)(int ch));

//void    _trace(const u8* pstr);
//void    _hexstrTrace(u8* pstr, u8* pnum, u16 size);
//void    _my_printf(const s8* str, ...);

void printf_string(int8_t * str);
void printf_char(int8_t c);
void printf_byte_hex(uint8_t a);
void printf_bytes_hex(uint8_t * str, uint32_t n);
void printf_byte_decimal(int8_t a);
void printf_16bit_hex(uint16_t aHalfWord);
void printf_32bit_hex(uint32_t aWord);

#endif /* _MY_PRINTF_H_ */
