#include "_my_printf.h"

void printf_char(char c)
{
	_my_fputc(c); // send next string character
}

void printf_string(char * str)
{
	while (*str != 0) {	      // while not reach the last string character
		_my_fputc(*str); // send next string character
		str++;
	}
}

void printf_byte_hex(uint8_t a)
{		  // print a Byte in hex format
	char b;
	b = ((0xF0 & a) >> 4);
	b += (b < 10) ? 48 : 55;
	_my_fputc(b);
	b = (0xF & a);
	b += (b < 10) ? 48 : 55;
	_my_fputc(b);
}

void printf_16bit_hex(uint16_t aHalfWord)
{
	printf_byte_hex((aHalfWord >> 8) & 0xFF);
	printf_byte_hex((aHalfWord) & 0xFF);
}

void printf_32bit_hex(uint32_t aWord)
{
	printf_byte_hex((aWord >> 24) & 0xFF);
	printf_byte_hex((aWord >> 16) & 0xFF);
	printf_byte_hex((aWord >> 8) & 0xFF);
	printf_byte_hex((aWord) & 0xFF);
}

void printf_bytes_hex(uint8_t * str, uint32_t n)
{
	uint32_t i;
	for (i = 0; i < n; i++) {
		printf_byte_hex(str[i]);
		_my_fputc(' ');
	}
	_my_fputc('\r');
	_my_fputc('\n');
}
