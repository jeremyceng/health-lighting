
#include "jxos_public.h"

#if ((JXOS_STANDARD_APP_BUTTON_SCAN_TASK_ENABLE == 1)&&(JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1))

#include "string.h"
#include "driver/button.h"

static swtime_type button_scan_swt;

#if (JXOS_MSG_ENABLE == 1)
static uint8_t button_msg_buff[BUTTON_SCAN_TASK_MSG_BUFF_LEN];
JXOS_MSG_HANDLE standard_app_button_scan_msg;
#endif
uint8_t standard_app_button_event_msg[BUTTON_SCAN_TASK_BUTTON_NUM_MAX];


#if (BUTTON_SCAN_TASK_LOW_POWER_CONSUMPTION_ENABLE == 1)
static JXOS_EVENT_HANDLE button_press_event;
void standard_app_button_task_button_press_interrupt_handler(void)
{
	jxos_event_set(button_press_event);
}
#endif

#if (JXOS_Compiler_optimization_1 == 1)
#if (JXOS_TASK_ENABLE == 1)
static void button_scan_task(void)
#else
void standard_app_button_scan_task(void)
#endif
#else
static void button_scan_task(uint8_t task_id, void * parameter)
#endif
{
#if (BUTTON_SCAN_TASK_LOW_POWER_CONSUMPTION_ENABLE == 1)
	if(jxos_event_wait(button_press_event) == 1){
		sys_software_timer_task_restart_timer(button_scan_swt);
		button_scan_handler();
		if(button_all_release() == 1){
			sys_software_timer_task_stop_timer(button_scan_swt);
		}
	}
#endif
	if(sys_software_timer_task_check_overtime_timer(button_scan_swt) == 1){
		button_scan_handler();
#if (BUTTON_SCAN_TASK_LOW_POWER_CONSUMPTION_ENABLE == 1)
		if(button_all_release() == 1){
			sys_software_timer_task_stop_timer(button_scan_swt);
		}
#endif
	}
}

/******************************/

static void send_button_msg(uint8_t button_num, uint8_t button_event)
{
#if (JXOS_MSG_ENABLE == 1)	
	STD_APP_BUTTON_SCAN_MSG_STRUCT msg_itme;
	msg_itme.button_num = button_num;
	msg_itme.button_event = button_event;
	jxos_msg_send(standard_app_button_scan_msg, &msg_itme);
#endif
	standard_app_button_event_msg[button_num] = button_event;
}

void standard_app_button_scan_task_button_press_event_handler(uint8_t button_num)
{
	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_PRESS);
}

void standard_app_button_scan_task_button_release_event_handler(uint8_t button_num)
{
	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_RELEASE);
}

void standard_app_button_scan_task_button_long_press_event_handler(uint8_t button_num)
{
	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_LONG_PRESS);
}

void standard_app_button_scan_task_button_long_press_repeat_event_handler(uint8_t button_num)
{
	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_LONG_PRESS_REPEAT);
}

/******************************/
void standard_app_button_scan_task_init(void)
{
	button_init();

#if (JXOS_TASK_ENABLE == 1)
	jxos_task_create(button_scan_task, "bt_scan", 0);
#endif
#if (JXOS_MSG_ENABLE == 1)
    standard_app_button_scan_msg = jxos_msg_create(button_msg_buff, BUTTON_SCAN_TASK_MSG_BUFF_LEN, sizeof(STD_APP_BUTTON_SCAN_MSG_STRUCT), "std_app_bt_scan");
#endif
	memset(standard_app_button_event_msg, 0xff, BUTTON_SCAN_TASK_BUTTON_NUM_MAX);

	button_scan_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(button_scan_swt, BUTTON_SCAN_TASK_SCAN_TICK_TIME_MS);
#if (BUTTON_SCAN_TASK_LOW_POWER_CONSUMPTION_ENABLE != 1)
		sys_software_timer_task_restart_timer(button_scan_swt);
#endif
}

#endif
