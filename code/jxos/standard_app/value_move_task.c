
#include "jxos_public.h"

#if ((JXOS_STANDARD_APP_VALUE_MOVE_TASK_ENABLE == 1)&&(JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1))
#include "driver/value_move.h"

#if (VALUE_MOVE_TASK_TICK_TIME_MS == 0)
#error VALUE_MOVE_TASK_TICK_TIME_MS can t be 0
#endif

static swtime_type value_move_swt;

#if (JXOS_Compiler_optimization_1 == 1)
#if (JXOS_TASK_ENABLE == 1)
static void value_move_task(void)
#else
void standard_app_value_move_task(void)
#endif
#else
static void value_move_task(uint8_t task_id, void * parameter)
#endif
{
	if(sys_software_timer_task_check_overtime_timer(value_move_swt) == 1){
		value_move_tick_handler();
	}
}

void standard_app_value_move_task_init(void)
{
	value_move_init();

#if (JXOS_TASK_ENABLE == 1)
	jxos_task_create(value_move_task, "v_move", 0);
#endif

	value_move_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(value_move_swt, VALUE_MOVE_TASK_TICK_TIME_MS);
	sys_software_timer_task_restart_timer(value_move_swt);
}

/******************************************************/
void standard_app_value_move_task_move_to(uint8_t num, uint16_t val, uint16_t move_time_ms)
{
	value_move_to_value_by_tick(num, val, move_time_ms/VALUE_MOVE_TASK_TICK_TIME_MS);
}

void standard_app_value_move_task_move_to_by_step(uint8_t num, uint16_t val, uint16_t steps)
{
	value_move_to_value_by_step(num, val, steps);
}

//move to the "max_val" in the first round
void standard_app_value_move_task_circle_move(uint8_t num, uint16_t max_val, uint16_t min_val, uint16_t circle_time_ms)
{
	value_move_circle_move_by_tick(num, max_val, min_val, circle_time_ms/VALUE_MOVE_TASK_TICK_TIME_MS);
}

//move to the "max_val" in the first round
void standard_app_value_move_task_circle_move_by_step(uint8_t num, uint16_t max_val, uint16_t min_val, uint16_t steps)
{
	value_move_circle_move_by_step(num, max_val, min_val, steps);
}

void standard_app_value_move_task_stop_move(uint8_t num)
{
	value_move_stop_move(num);
}

#endif
