
#ifndef __standard_app_BUTTON_TASK_H
#define __standard_app_BUTTON_TASK_H

#include "../lib/type.h"

void standard_app_button_scan_task_init(void);
void standard_app_button_scan_task(void);

#endif
