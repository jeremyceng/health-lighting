



#define BUTTON_NUM_MAX 		                3
#define BUTTON_PRESS_LEVEL				    0

#define BUTTON_JITTER_TICK					0
#define BUTTON_LONG_PRESS_TICK 		        100
#define BUTTON_LONG_PRESS_REPEAT_TICK 		30


#define button_hal_init_config()                    //mcu_init_button_pin()
#define button_read_pin_level_config(button_num)    //mcu_read_button_pin(button_num)

#define button_free_event(button_num)               
#define button_press_event(button_num)
#define button_release_event(button_num)
#define button_long_press_event(button_num)
#define button_long_press_repeat_event(button_num)