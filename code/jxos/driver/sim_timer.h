
#ifndef __SIM_TIEMR__H
#define __SIM_TIEMR__H

#include "../lib/type.h"


void sim_timer_init(void);
void sim_timer_tick_hander(void);

uint8_t sim_timer_check_running(uint8_t sim_timer_num);
uint8_t sim_timer_check_overtime(uint8_t sim_timer_num);

void sim_timer_start(uint8_t sim_timer_num);
void sim_timer_stop(uint8_t sim_timer_num);
void sim_timer_restart(uint8_t sim_timer_num);
void sim_timer_set_timeout(uint8_t sim_timer_num, uint16_t timeout);

#endif