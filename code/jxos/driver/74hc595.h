
#ifndef __74HC595__H
#define __74HC595__H

#include "../lib/type.h"

void hc_74hc595_output(uint8_t dat); //bit0...bit7 --> Qa...Qh

#endif
