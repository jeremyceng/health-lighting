
#include "../lib/type.h"
#include "74hc595_lite_config.h"

void hc_74hc595_output(uint8_t dat) //bit0...bit7 --> Qa...Qh
{
    uint8_t i;
    hc_74hc595_sclr_hihgt_config();
    hc_74hc595_oe_low_config();
    hc_74hc595_rck_low_config();
    hc_74hc595_sck_low_config();

	for(i = 0; i < 8; i++){
 		if((dat&0x80) == 0)
			hc_74hc595_si_low_config();
		else
			hc_74hc595_si_hihgt_config(); 
      
        dat <<= 1;
        hc_74hc595_delay_config();
        hc_74hc595_sck_hihgt_config();
        
        hc_74hc595_delay_config();
        hc_74hc595_sck_low_config();
	}
    
    hc_74hc595_delay_config();
    hc_74hc595_rck_hihgt_config();
    hc_74hc595_delay_config();
    hc_74hc595_rck_low_config();
}
