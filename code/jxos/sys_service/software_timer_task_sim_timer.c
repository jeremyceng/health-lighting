
#include <jxos_config.h>

#if (JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1)
#include "../driver/sim_timer.h"

void (*sys_software_timer_task_hal_init_callback)(void) = 0;
void (*sys_software_timer_task_hal_start_callback)(void) = 0;
void (*sys_software_timer_task_hal_stop_callback)(void) = 0;

static uint8_t sim_timer_index[SWTIMER_MAX];
static uint8_t sim_timer_index_count;
bool_t software_timer_task_tick_handler_flag;

void sys_software_timer_task_init(void)
{
	if(sys_software_timer_task_hal_init_callback != 0){
		sys_software_timer_task_hal_init_callback();
	}

	software_timer_task_tick_handler_flag = false;

    sim_timer_init();
    sim_timer_index_count = 0;

	if(sys_software_timer_task_hal_start_callback != 0){
		sys_software_timer_task_hal_start_callback();
	}
}

void sys_software_timer_task(void)
{
	if(software_timer_task_tick_handler_flag == true){
		software_timer_task_tick_handler_flag = false;
        sim_timer_tick_hander();
    }
}

uint8_t sys_software_timer_task_new_timer(void)
{
    uint8_t index = 0;
    if(sim_timer_index_count < SWTIMER_MAX){
        sim_timer_index_count++;
        index = sim_timer_index_count;
    }
    return index;
}

void sys_software_timer_task_set_timer(uint8_t swtime, uint16_t time)
{   
    swtime -= 1;
    if(swtime < sim_timer_index_count){
        time = time/TIMER_CONSTANT_INTERVAL_MS;
        sim_timer_set_timeout(swtime, time);
    }
}

void sys_software_timer_task_start_timer(uint8_t swtime)
{
    swtime -= 1;
    if(swtime < sim_timer_index_count){
        sim_timer_start(swtime);
    }
}

void sys_software_timer_task_stop_timer(uint8_t swtime)
{
    swtime -= 1;
    if(swtime < sim_timer_index_count){
        sim_timer_stop(swtime);
    }
}

void sys_software_timer_task_restart_timer(uint8_t swtime)
{
    swtime -= 1;
    if(swtime < sim_timer_index_count){
        sim_timer_restart(swtime);
    }
}

uint8_t sys_software_timer_task_check_running_timer(uint8_t swtime)
{
    swtime -= 1;
    if(swtime < sim_timer_index_count){
        swtime = sim_timer_check_running(swtime);
    }
    return swtime;
}

uint8_t sys_software_timer_task_check_overtime_timer(uint8_t swtime)
{
    swtime -= 1;
    if(swtime < sim_timer_index_count){
        swtime = sim_timer_check_overtime(swtime);
    }
    return swtime;
}

#endif
