
#include <jxos_config.h>

#if (JXOS_TASK_ENABLE == 1)
#include "kernel/jxos_task.h"
#endif
#if (JXOS_ENENT_ENABLE == 1)
#include "kernel/jxos_event.h"
#endif
#if (JXOS_MSG_ENABLE == 1)
#include "kernel/jxos_msg.h"
#endif
#if (JXOS_MESSAGE_PIPE_ENABLE == 1)
#include "kernel/jxos_message_pipe.h"
#endif
#if (JXOS_BULLETIN_BOARD_ENABLE == 1)
#include "kernel/jxos_bulletin_board.h"
#endif
#if (JXOS_MAIL_BOX_ENABLE == 1)
#include "kernel/jxos_mail_box.h"
#endif

#if (JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1)
#include "sys_service/software_timer_task.h"
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
#include "sys_service/debug_print_task.h"
#endif
#if (JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE == 1)
#include "sys_service/power_mgr_task.h"
#endif

#if (JXOS_STANDARD_APP_KEY_TASK_ENABLE == 1)
#include "standard_app/key_task.h"
#endif
#if (JXOS_STANDARD_APP_LED_TASK_ENABLE == 1)
#include "standard_app/led_task.h"
#endif
#if (JXOS_STANDARD_APP_BUTTON_SCAN_TASK_ENABLE == 1)
#include "standard_app/button_scan_task.h"
#endif
#if (JXOS_STANDARD_APP_BUTTON_TASK_ENABLE == 1)
#include "standard_app/button_task.h"
#endif
#if (JXOS_STANDARD_APP_FRAME_SEND_TASK_ENABLE == 1)
#include "standard_app/frame_send_task.h"
#endif
#if (JXOS_STANDARD_APP_COMPOSITE_KEY_TASK_ENABLE == 1)
#include "standard_app/composite_key_task.h"
#endif
#if (JXOS_STANDARD_APP_VALUE_MOVE_TASK_ENABLE == 1)
#include "standard_app/value_move_task.h"
#endif

void jxos_hal_init(void);
void jxos_user_task_init(void);
void jxos_prepare_to_run(void);
void jxos_user_task_run(void);

void jxos_run(void)
{
	//OS init
#if (JXOS_TASK_ENABLE == 1)
    jxos_task_init();
#endif
#if (JXOS_ENENT_ENABLE == 1)
    jxos_event_init();
#endif // JXOS_ENENT_ENABLE
#if (JXOS_MSG_ENABLE == 1)
    jxos_msg_init();
#endif // JXOS_MSG_ENABLE
#if (JXOS_MESSAGE_PIPE_ENABLE == 1)
	jxos_message_pipe_init();
#endif
#if (JXOS_BULLETIN_BOARD_ENABLE == 1)
	jxos_bulletin_board_init();
#endif
#if (JXOS_MAIL_BOX_ENABLE == 1)
	jxos_mail_box_init();
#endif

	//OS hal init
	jxos_hal_init();

	//Task init
#if (JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1)
	sys_software_timer_task_init();
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
	sys_debug_print_task_init();
#endif

	//standard_app init
#if (JXOS_STANDARD_APP_KEY_TASK_ENABLE == 1)
	standard_app_key_task_init();
#endif
#if (JXOS_STANDARD_APP_LED_TASK_ENABLE == 1)
	standard_app_led_task_init();
#endif
#if (JXOS_STANDARD_APP_BUTTON_SCAN_TASK_ENABLE == 1)
	standard_app_button_scan_task_init();
#endif
#if (JXOS_STANDARD_APP_BUTTON_TASK_ENABLE == 1)
	standard_app_button_task_init();
#endif
#if (JXOS_STANDARD_APP_FRAME_SEND_TASK_ENABLE == 1)
    standard_app_frame_send_task_init();
#endif
#if (JXOS_STANDARD_APP_COMPOSITE_KEY_TASK_ENABLE == 1)
    standard_app_composite_key_task_init();
#endif
#if (JXOS_STANDARD_APP_VALUE_MOVE_TASK_ENABLE == 1)
	standard_app_value_move_task_init();
#endif

	//user_task init
	jxos_user_task_init();

#if (JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE == 1)
	sys_power_mgr_task_init();
#endif

	jxos_prepare_to_run();

#if (JXOS_TASK_ENABLE == 1)
	//OS run
	jxos_task_schedule();
#else
	while (1)
	{
#if (JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1)
	sys_software_timer_task();
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
	sys_debug_print_task_init();
#endif

#if (JXOS_STANDARD_APP_KEY_TASK_ENABLE == 1)
	standard_app_key_task();
#endif
#if (JXOS_STANDARD_APP_LED_TASK_ENABLE == 1)
	standard_app_led_task();
#endif
#if (JXOS_STANDARD_APP_BUTTON_SCAN_TASK_ENABLE == 1)
	standard_app_button_scan_task();
#endif
#if (JXOS_STANDARD_APP_BUTTON_TASK_ENABLE == 1)
	standard_app_button_task();
#endif
#if (JXOS_STANDARD_APP_FRAME_SEND_TASK_ENABLE == 1)
    standard_app_frame_send_task();
#endif
#if (JXOS_STANDARD_APP_COMPOSITE_KEY_TASK_ENABLE == 1)
    standard_app_composite_key_task();
#endif
#if (JXOS_STANDARD_APP_VALUE_MOVE_TASK_ENABLE == 1)
	standard_app_value_move_task();
#endif

	//user_task run
	jxos_user_task_run();

#if (JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE == 1)
	sys_power_mgr_task();
#endif
	}
#endif
}
