/*************************************************************************************
* KF8V系列单片机开发板演示程序
* 标         题: ADC转换采样
* 项  目  名: 08-ADC_TEST
* 开发环境：ChipON IDE
* 版          本： V2.0 (2015/10/09)
* 使用芯片：KF8V216
* 作          者：上海芯旺微电子技术有限公司
* 功能简述: ADC转换采样，数码管显示采样得到的值
* 跳线接法：单片机的P00、P01 P04 P05(位选) P1口（段选）跳上跳线帽，单片机的P02端（模拟输入口）、 J14跳上跳线帽
* 如需下载程序，则需要把单片机的P03、VDD、VSS跳上跳线帽，并把P00、P01的跳线帽移除
*************************************************************************************/
#include<KF8V216.h>
/***************宏定义**********************/
#define uchar unsigned char
#define uint  unsigned int

#define	 NUM1_IO	P0LR4
#define	 NUM2_IO	P0LR5
#define	 NUM3_IO	P0LR1
#define	 NUM4_IO	P0LR0

/******************宏定义结束*****************/
/**************定义全局变量（注：为节省内存空间，全局变量尽量不要赋初值）*********/
uchar const Arr_num[] = {
					0xE7,		//		;0
					0x06,		//		;1
					0xB3,		//		;2
					0x37,		//		;3
					0x56,		//		;4
					0x75,		//		;5
					0xF5,		//		;6
					0x07,		//		;7
					0xF7,		//		;8
					0x77,		//		;9
					0xD7,		//		;A
					0xF4,		//		;B
					0xE1,		//		;C
					0xB6,		//		;D
					0xF1,		//		;E
					0xD1,		//		;F
					0X00		//		;空
};                                        //定义数码管可以显示的0——f,共阴接法
uchar Unit, Decade, Hundred, Thousand;    //定义数码管的个十百千位

/***********************全局变量定义结束**************************************/
/*****************函数声明(注：中断和main函数不需声明)*************************/
void Init_fun();
void Delay_200us();
uint Adc_fun(void);
uint Adc_read(void);
void Display();
/********************函数声明结束************************************/

/************************
 * 函数名     ：main
 * 函数功能：程序入口主函数
 * 入口参数：无
 * 返回          ：无
 ************************/

void main()
{
	uint Dis_adc = 0 ;		            //采样接收变量
	Init_fun();			                //初始化
	Delay_200us();
	Dis_adc = 0x1111;
	while(1)
	{
		Dis_adc = Adc_read();           //将ADC采样返回值赋予Dis_adc
		Unit = Dis_adc&0x0f;            //将Dis_adc的最低4位赋予数码管的个位
		Decade = Dis_adc&0xf0;
		Decade >>= 4;                   //将Dis_adc的第7-4位赋予数码管的十位
		Dis_adc >>= 8;                  //右移8位，取Dis_adc的高8位
		Hundred = Dis_adc&0x0f;         //将此时的Dis_adc的最低4位赋予数码管的百位
		Thousand = Dis_adc&0xf0;
		Thousand >>= 4;                 //将此时的Dis_adc的第7-4位赋予数码管的千位
		Display();                      //显示
	}
}

/************************
 * 函数名     ：Init_fun
 * 函数功能：初始化函数
 * 入口参数：无
 * 返回          ：无
 ************************/

void Init_fun()
{
	OSCCTL = 0x60;                     //设置为8M
	/*****端口初始化*****/
	TR0 = 0x0C;				           //设置P03端口为输入  P02为AN2模拟通道 配置为输入口
	TR1 = 0x00;				           //设置P1端口为输出

	P0 = 0;
	P1 = 0;

	P0LR = 0;
	P1LR = 0;

	/****初始化AD寄存器****/
	ANSEL = 0x04;		              //设置通道2为模拟口
	ADCCTL0 = 0x89;		              //右对齐，通道2， AD使能打开
	ADCCTL1 = 0x17;		              //8分频，VDD作为参考电压
	PCTL = 0X00;                      //禁止中断优先级 IPEN = 0
}
/************************
 * 函数名     ：Adc_fun
 * 函数功能：ADC函数，模数转换
 * 入口参数：无
 * 返回          ：返回ADC采样值
 ************************/
uint Adc_fun(void)
{
	    uint Adc_num = 0 ;			  //ADC转换缓冲变量
		START = 1;					  //启动ADC
		while(START);                 //等待转换结束

/*************************************结果右对齐********************************************/
		Adc_num = ADCDATAH&0x0F;	  //将高位加进去
		Adc_num <<= 8;		          //左移动8位高位归位
		Adc_num |= ADCDATAL;		  //将低位加进去
/*************************************结果右对齐********************************************/

		return Adc_num;			      //返回转换值

}
/************************
 * 函数名     ：Adc_read
 * 函数功能：ADC求均值函数，adc读取函数
 * 入口参数：无
 * 返回          ：返回ADC采样8次均值
 ************************/
uint Adc_read(void)
{
	uint Adc_sum = 0 ; 		          //adc采样累加变量
	uchar i = 0;
	for(i = 0; i < 8; i++)
	{
		Adc_sum += Adc_fun();		  //累加八次采样值
	}
	Adc_sum >>= 3;		              //右移动3位  除8求均值
	return Adc_sum;
}

/************************
 * 函数名     ：Display
 * 函数功能：数码管显示
 * 入口参数：显示数字
 * 返回          ：无
 ************************/
void Display()
 {
	uchar i;
	for(i = 0; i < 50 ; i++)
	 {
	    NUM1_IO=0;
		P1LR=Arr_num[Unit];
		Delay_200us();
		P1LR=0;
		NUM1_IO=1;

		NUM2_IO=0;
		P1LR=Arr_num[Decade];
		Delay_200us();
		P1LR=0;
		NUM2_IO=1;

		NUM3_IO=0;
		P1LR=Arr_num[Hundred];
		Delay_200us();
		P1LR=0;
		NUM3_IO=1;

		NUM4_IO=0;
		P1LR=Arr_num[Thousand];
		Delay_200us();
		P1LR=0;
		NUM4_IO=1;
	}
}


/************************
 * 函数名     ：Delay_200us
 * 函数功能：短时间延时
 * 入口参数：无
 * 返回          ：无
 ************************/
void Delay_200us()
{
	uchar i = 60;
	while(--i);
}
//中断函数0:0X04入口地址
void int_fun0() __interrupt (0)
{

}


//中断函数1:0x14入口地址
void int_fun1() __interrupt (1)
{

}

