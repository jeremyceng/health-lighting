/****************************************************************************************
 *
 * 文  件  名: main.c
 * 项  目  名: 11_Usart
 * 版         本: v1.0
 * 日         期: 2016年05月16日 11时38分22秒
 * 作         者: Administrator
 * 程序说明：通用全双工/半双工收发器
 * 适用芯片：KF8FXXX系列——KF8F212、KF8F214、KF8F212、KF8F222、KF8F232、KF8F233、KF8F304、
 * 						 KF8F312、KF8F321、KF8F323、KF8F324、KF8F334、KF8F335、KF8F336
 * 			KF8FXXXX系列——KF8F2156、KF8F3156、KF8F4156、KF8F3155、KF8F4155
 * 			KF8SXXXX系列——KF8S1005、KF8S1006、KF8S1010、KF8S1011
 * 			KF8TS27XX系列——KF8TS2702、KF8TS2708、KF8TS2710、KF8TS2714、KF8TS2716
 * 			KF8VXXX系列——KF8V427、KF8V429
 ****************************************************************************************/
#include<KF8S1011.h>
#define uchar unsigned char
#define uint  unsigned int

#define Sent_Vaule_To_UART(key_buf)   {TXSDR=key_buf; while(TXSRS==0);}// 调试时串口输出

unsigned char Timer_Count;
unsigned char Send_Date;
unsigned char Send_Flag;

unsigned char Rev_Temp = 0;
unsigned char Rev_Flag;

/****************************************************************************************
 * 函数名     ：Delay_ms
 * 函数功能：长时间延时
 * 入口参数：延时基数 uchar ms_data
 * 返回          ：无
 ****************************************************************************************/
void Delay_ms(uint ms_data)
{
	uchar i;
	while(ms_data--)
	{
		i = 124;
		while(i--);
	}
}

/****************************************************************************************
 * 函数名     ：init_mcu
 * 函数功能：mcu初始化函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void init_mcu()
{
	/***时钟初始化****/
	OSCCTL = 0x60;          //设置为8M

	/***端口初始化****/
	TR0 = 0x0B;				//设置P03端口为输入，P0其他I/O口为输出
	TR1 = 0x00;				//设置P1端口为输出
	TR2 = 0x00;				//设置P2端口为输出
	TR3 = 0x00;				//设置P3端口为输出
	TR4 = 0x00;				//设置P4端口为输出

	P0  = 0;				//P0口输出低电平
	P1  = 0;				//P1口输出低电平
	P2  = 0;				//P2口输出低电平
	P3  = 0;				//P3口输出低电平
	P4  = 0;				//P4口输出低电平

	OPTR = 0x05;	//定时器0,定时发送数据到端口 计时64分频
	T0 = 0x64;		// 可计时156个周期，结合分频64和时钟8M实现时间间隔定时 约  5ms
}

/****************************************************************************************
 * 函数名     ：init_usart
 * 函数功能：Usart初始化函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void init_usart()
{
	/***Usart相关寄存器初始化****/
	BRCTL=0x00;		// 	接收空闲  使用8位计数器，不考虑唤醒是使能和自动波特率检测

	EUBRGH=0x00;	// 波特率公式=SCLK/(m*y+1),其中SCLK系统时钟，如这里8M，m为倍频数，见手册，y为8位或16位波特率计数值
	EUBRGL=0x0C; 	// SYNC=0(全双工异步模式),BRG16=0(使用8位波特率发生器),HBRG=0(低速)，Baud=8000000/(64*(12+1))=9615

	SYNC=1;			//半双工同步模式
	CSRS=1;			//主模式
	SPEN=1;			//使能串行口
	SRXEN=0;
	CRXEN=0;		//禁止接收模式
	TXEN=1;			//使能发送模式

	TXIF=0;
	TXIE=0;
	RXIF=0;
	RXIE=0;

	INTCTL = 0X60;     // 使能外部中断 PUIE 使能总中断 AIE  T0IE

}

/****************************************************************************************
 * 函数名     ：usart_function
 * 函数功能：Usart功能函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void usart_function()
{
	if(Send_Flag)
	{
		Send_Flag=0;

		SRXEN=0;
		CRXEN=0;	//禁止接收模式
		TXEN=1;		//使能发送模式

		Sent_Vaule_To_UART(Send_Date);



		SYNC=1;			//半双工同步模式
		CSRS=1;			//主模式
		SPEN=1;			//使能串行口
		TXEN=0;			//禁止发送模式
		RXIF=0;
		SRXEN=1;
		CRXEN=0;		//启动接收模式

		while(!RXIF);
		Rev_Temp=RXSDR;
		RXIF=0;

		switch(Rev_Temp)
		{
		case 1:P37=!P37;break;
		case 2:P36=!P36;break;
		case 3:P35=!P35;break;
		case 4:P34=!P34;break;
		case 5:P33=!P33;break;
		case 6:P30=!P30;break;
		default:P37=1;P36=1;P35=1;P34=1;P33=1;P30=1;break;
		}

		AIE=0;
						while(1);
	}
}

/****************************************************************************************
 * 函数名     ：main
 * 函数功能：程序入口主函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void main()
{
	init_mcu();
	init_usart();
	Delay_ms(100);
	AIE=1;

	while(1)
	{
		usart_function();
	}
}

//中断函数
void int_fun() __interrupt
{
	if(RXIF)
	{
		if(OVFER || FRER)
		{
			Rev_Flag=0;
		}
		else
		{
			Rev_Flag=1;
		}

		Rev_Temp=RXSDR;  // 清零 RXIF
	}

	if(T0IF)
	{
		T0IF = 0;
		T0 = 0x64;
		Timer_Count++;

		if(Timer_Count>100)
		{
			Timer_Count=0;
			Send_Date++;
			if(Send_Date>7)Send_Date=1;
			Send_Flag=1;
		}
	}
}
