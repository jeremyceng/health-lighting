/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: 05-TIME4_TESE
 * 版 本: v1.0
 * 日 期: 2016年05月31日 15时33分45秒
 * 作 者: Administrator
 * 程序说明:2716 16位定时器T4定时（未使用触摸情况下）。
 * 适用芯片: KF8F3155，KF8F4155，KF8TS2716，KF8TS2516
 ****************************************************************************************/
#include<KF8TS2716.h>
/****************************************************************************************
 * 函数名：   init_mcu
 * 函数功能：mcu初始化函数
 * 入口参数：无
 * 返回：       无
 ****************************************************************************************/
void init_mcu()
{
	/***时钟初始化****/
	OSCCTL = 0x60;          //设置为8M
	/***端口初始化****/
	TR0 = 0X04;            //P02设置为输入，其余口为输出
	TR1 = 0x00;            //P1设置为输出
	TR2 = 0X00;            //P2设置为输出
	TR3 = 0X00;            //P3设置为输出

    P0LR=0x00;				//P0输出低
    P1LR=0x00;              //P1输出低
    P2LR=0x00;				//P2输出低
    P3LR=0x00;				//P3输出低

    P0=0x00;
    P1=0x00;
    P2=0x00;
    P3=0x00;
}
/****************************************************************************************
 * 函数名：   init_T4
 * 函数功能：T3初始化函数
 * 入口参数：无
 * 返回：       无
 ****************************************************************************************/
void init_T4()
{
	T4H = 255;
	T4L = 155;	  //定时25us

	T4ON = 1;
	T4IE = 1;
	T4IF = 0;
 	PUIE = 1;      //使能外设中断
 	AIE = 1;
}
//主函数
void main()
{
	init_mcu();
	init_T4();
	while(1)
	{
     _CWDT();
	}
}
//中断函数
void int_fun() __interrupt (0)
{;
	if(T4IF)
	{
		T4IF=0;
		T4H = 255;
		T4L = 155;	  //定时25us
		P1LR3=!P1LR3;
	}
}


