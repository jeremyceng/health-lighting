/*************************************************************************************
* KF8F系列单片机开发板演示程序
* 标         题: LED硬件驱动
* 项  目  名: 13-LED-Drive_TEST
* 开发环境：ChipON IDE
* 版          本： V1.0 (2015/10/09)
* 使用芯片：KF8F4156
* 作          者：上海芯旺微电子技术有限公司
* 功能简述: 实现LED显示的开启和关闭，实现模拟定时下的计时累加
*
* 跳线接法：需给单片机供电,同时连接JP2 以及码输出P3口和段输出P00 P01 P03 P04，
*				可以连接P12和J21  当LED显示时灭灯，关闭时亮灯
* 特殊说明：这里采用LED共阴极模块，同时电源应该选择5V工作。
*************************************************************************************/
#include<KF8F4156.h>


//0x80 1段共阴  0x83 1段共阳;
//0x84 2段共阴  0x87 2段共阳;
//0x88 3段共阴  0x8B 3段共阳;
//0x8C 4段共阴  0x8F 4段共阳;
//0x90 5段共阴  0x93 5段共阳;
//0x94 6段共阴  0x97 6段共阳;
//0x98 7段共阴  0x9B 7段共阳;
//0x9C 8段共阴  0x9F 8段共阳;
#define  LED_WORK_MODE	0x8C

unsigned char  L_1,L_2,L_3,L_4;  // 目标显示值
unsigned int  timer;			 // 时间计数
unsigned char work_flag=1;		// 显示与否的标志

unsigned char ARR_List[18]={        // 0亮 1灭 码值与引脚连接相关
							0xC0,		// 0
							0xF9,		// 1
							0xA4,		// 2
							0xB0,		// 3
							0x99,		// 4
							0x92,		// 5
							0x82,		// 6
							0xF8,		// 7
							0x80,		// 8
							0x90,		// 9
							0x88,		// a
							0x83,		// b
							0xA7,		// c
							0xA1,		// d
							0x86,		// e
							0x8E,		// f
							0xBF,		// -
							0xFF,		// 空
};
//;******************************************************************************
//;* 函 数 名: init_mcu()
//;* 函数功能: 初始化
//;* 入口参数: 无
//;* 返    回: 无
//;******************************************************************************
void init_mcu()
{
	//晶振频率选择
	OSCCTL = 0x60;           //8M

	//端口初始化
	TR0 = 0x04;
	TR1 = 0x00;
	TR2 = 0x00;
	TR3 = 0x00;  	// LED输出

	P0LR = 0X1B;	// COM 端高电平
	P1LR = 0X00;
	P2LR = 0X00;
	P3LR = 0X00;

	OPTR=0x44;		//使能上拉，T0计时模式，等待启动

	AIE=1;
}
//;************************************************************************************
//;* 函 数 名:  led_init
//;* 函数功能: 选择参数
//;* 入口参数:
//;* 返    回:
//;************************************************************************************
void led_init()
{
	//输出模式选项 ： 0 ——推挽  	1 ——开漏
	LEDOMS0=0x00;			//LED-COMx端输出模式设置位
	LEDOMS1=0x00;			//LED-SEGx端输出模式设置位
	//开漏方式选择：  1——P管	  	0——N管
	LEDODS0=0xFF;			//LED-COMx端开漏方式选择位
	LEDODS1=0xFF;			//LED-SEGx端开漏方式选择位
	// 辉度调节 0 15:16  1 14:16  2 12:16  3  10:16
	// 辉度调节 4 8:16   5 6:16   6 4:16   7  2:16
	LEDLUM=0x01;
	// 扫描频率： B7-B4 时钟分频  B3-B0 预分频 更多见数据手册
	// CLK(led)=SCLK  /  (   ((B3~B0)+1)  *   POWER(2,(B7~B4))  )
	// 视觉停留需要至少50HZ不闪烁，LED模块内部还有1个32分频，
	// 因此4段下需要4*50*32=6.4k的设定频率
	// 因此8段下需要8*50*32=12.8k的设定频率
	LEDPRE=0x72;

	LEDDATA3=ARR_List[10];								//千位
	LEDDATA2=ARR_List[11];		 						//百位
	LEDDATA1=ARR_List[12];					 			//十位
	LEDDATA0=ARR_List[13];  //   0 亮 1灭，与共阴共阳无关 	//个位

	LEDCTL=LED_WORK_MODE;  // 默认开启 显示  ABCD
	//while(1);
}
//;************************************************************************************
//;* 函 数 名:  Start_for_led
//;* 函数功能: 启动LED的显示
//;* 入口参数:
//;* 返    回:
//;************************************************************************************
void Start_for_led()
{
	//计数初始值
	L_1=9;
	L_2=9;
	L_3=8;
	L_4=7;
	timer=0;
	// 启动LED
	LEDCTL=LED_WORK_MODE;
	// 启动计时 计数
	T0=0x64;
	T0IE=1;
	work_flag=1;
}
//;************************************************************************************
//;* 函 数 名:  Stop_for_led
//;* 函数功能: 关闭LED的显示
//;* 入口参数:
//;* 返    回:
//;************************************************************************************
void Stop_for_led()
{
	T0IE=0;
	LEDCTL=0x00;
	work_flag=0;
}
//主函数
void main()
{
	unsigned int timeA=0,timeB=0;

	init_mcu();
	led_init();
	Start_for_led();// 优先LED显示
	P1LR2=0;
	while(1)
	{
		LEDDATA3=ARR_List[L_1];
		LEDDATA2=ARR_List[L_2];
		LEDDATA1=ARR_List[L_3];
		LEDDATA0=ARR_List[L_4];

		// 开启关闭切换
		timeA++;
		if(timeA>20000)
		{
			timeA=0;
			timeB++;
			if(timeB>5)
			{
				timeB=0;
				if(!work_flag)
				{
					Start_for_led();
					P1LR2=0;
				}
				else
				{
					Stop_for_led();
					P1LR2=1;
				}
			}
		}
	}
}
//===================================================
//中断函数0:0X04入口地址
void int_fun0() __interrupt (0)
{
//===================================================
	if(T0IF)
	{
		T0IF=0;
		T0=0x64;

		timer++;
		if(timer>80)	// 加快慢控制
		{
			timer=0;
			//------
			L_4++;
			if(L_4==10)
			{
				L_4=0;
				L_3++;
				if(L_3==10)
				{
					L_3=0;
					L_2++;
					if(L_2==10)
					{
						L_2=0;
						L_1++;
						if(L_1==10)
						{
						   L_1=0;
						}
					}
				}
			}
		}
	}
//===================================================
}
//中断函数1:0x14入口地址
void int_fun1() __interrupt (1)
{

//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
}
