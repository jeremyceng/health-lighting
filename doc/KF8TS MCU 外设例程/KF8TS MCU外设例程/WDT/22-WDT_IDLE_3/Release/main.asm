;--------------------------------------------------------
; File Created by ChiponCC : aolisn inc.
; Version 3.0.4#6752 (Mar  1 2016 15:13:08) (MSVC)
; This file was generated Thu Jun 02 09:34:47 2016
;--------------------------------------------------------
; chipon port for the 16-bit core
;--------------------------------------------------------
;	.file	"../main.c"
	.radix dec
	.include "KF8TS2716.inc"
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.extern	_PSW_bits
	.extern	_P0_bits
	.extern	_P2_bits
	.extern	_P1_bits
	.extern	_P3_bits
	.extern	_INTCTL_bits
	.extern	_EIF1_bits
	.extern	_EIF2_bits
	.extern	_T1CTL_bits
	.extern	_PWMCTL_bits
	.extern	_BANK_bits
	.extern	_ADSCANCTL_bits
	.extern	_ANSEH_bits
	.extern	_ADCCTL0_bits
	.extern	_OPTR_bits
	.extern	_IP0_bits
	.extern	_IP1_bits
	.extern	_IP2_bits
	.extern	_TR0_bits
	.extern	_TR2_bits
	.extern	_TR1_bits
	.extern	_OSCSTA_bits
	.extern	_IP3_bits
	.extern	_VRECTL_bits
	.extern	_EIE1_bits
	.extern	_EIE2_bits
	.extern	_PCTL_bits
	.extern	_OSCCTL_bits
	.extern	_ANSEL_bits
	.extern	_PUR0_bits
	.extern	_IOCL_bits
	.extern	_ADCCTL1_bits
	.extern	_P0LR_bits
	.extern	_P2LR_bits
	.extern	_P1LR_bits
	.extern	_P3LR_bits
	.extern	_TR3_bits
	.extern	_EIE3_bits
	.extern	_EIF3_bits
	.extern	_CTCTL0_bits
	.extern	_ADCINTCTL_bits
	.extern	_PUR1_bits
	.extern	_PUR2_bits
	.extern	_CTCTL1_bits
	.extern	_INTEDGCTL_bits
	.extern	_VBIASCTL_bits
	.extern	_VBIAS1EN_bits
	.extern	_VBIAS2EN_bits
	.extern	_VDAC_bits
	.extern	_RSCTL_bits
	.extern	_BRCTL_bits
	.extern	_TSCTL_bits
	.extern	_SSCICTL0_bits
	.extern	_SSCICTL1_bits
	.extern	_SSCISTA_bits
	.extern	_SSCIMSK_bits
	.extern	_WDTPS_bits
	.extern	_MULCTL_bits
	.extern	_DIVCTL_bits
	.extern	_LEDCTL_bits
	.extern	_LEDPRE_bits
	.extern	_LEDDATA0_bits
	.extern	_LEDDATA1_bits
	.extern	_LEDDATA2_bits
	.extern	_LEDDATA3_bits
	.extern	_LEDDATA4_bits
	.extern	_LEDDATA5_bits
	.extern	_LEDDATA6_bits
	.extern	_LEDDATA7_bits
	.extern	_LEDOMS1_bits
	.extern	_LEDODS0_bits
	.extern	_LEDOMS0_bits
	.extern	_LEDODS1_bits
	.extern	_LEDLUM_bits
	.extern	_T4CTL_bits
	.extern	_T0
	.extern	_PCL
	.extern	_PSW
	.extern	_P0
	.extern	_P2
	.extern	_P1
	.extern	_P3
	.extern	_PCH
	.extern	_INTCTL
	.extern	_EIF1
	.extern	_EIF2
	.extern	_T1L
	.extern	_T1H
	.extern	_T1CTL
	.extern	_PWM1L
	.extern	_PWM1H
	.extern	_PWMCTL
	.extern	_PP1
	.extern	_BANK
	.extern	_ADSCANCTL
	.extern	_ANSEH
	.extern	_ADCDATA0H
	.extern	_ADCCTL0
	.extern	_OPTR
	.extern	_IP0
	.extern	_IP1
	.extern	_IP2
	.extern	_TR0
	.extern	_TR2
	.extern	_TR1
	.extern	_OSCSTA
	.extern	_IP3
	.extern	_VRECAL1
	.extern	_VRECTL
	.extern	_EIE1
	.extern	_EIE2
	.extern	_PCTL
	.extern	_OSCCTL
	.extern	_OSCCAL0
	.extern	_ANSEL
	.extern	_PP2
	.extern	_PWM2L
	.extern	_PWM2H
	.extern	_PUR0
	.extern	_IOCL
	.extern	_OSCCAL1
	.extern	_NVMDATAH
	.extern	_NVMDATAL
	.extern	_NVMADDRH
	.extern	_NVMADDRL
	.extern	_NVMCTL0
	.extern	_NVMCTL1
	.extern	_ADCDATA0L
	.extern	_ADCCTL1
	.extern	_P0LR
	.extern	_P2LR
	.extern	_P1LR
	.extern	_P3LR
	.extern	_TR3
	.extern	_EIE3
	.extern	_EIF3
	.extern	_OSCCAL2
	.extern	_OSCCAL3
	.extern	_T3L
	.extern	_ADCDATA1H
	.extern	_ADCDATA1L
	.extern	_CTCTL0
	.extern	_ADCINTCTL
	.extern	_ADCDATA2H
	.extern	_ADCDATA2L
	.extern	_ADCDATA3H
	.extern	_T3H
	.extern	_PUR1
	.extern	_PUR2
	.extern	_CTCTL1
	.extern	_INTEDGCTL
	.extern	_ADCDATA3L
	.extern	_VBIASCTL
	.extern	_VBIAS1EN
	.extern	_VBIAS2EN
	.extern	_VDAC
	.extern	_RSCTL
	.extern	_TXSDR
	.extern	_RXSDR
	.extern	_BRCTL
	.extern	_TSCTL
	.extern	_EUBRGL
	.extern	_EUBRGH
	.extern	_SSCICTL0
	.extern	_SSCICTL1
	.extern	_SSCISTA
	.extern	_SSCIBUFR
	.extern	_SSCIMSK
	.extern	_WDTPS
	.extern	_MULAH
	.extern	_MULAL
	.extern	_MULBH
	.extern	_MULBL
	.extern	_MULCTL
	.extern	_MULRES3
	.extern	_MULRES2
	.extern	_MULRES1
	.extern	_MULRES0
	.extern	_DIVCTL
	.extern	_DIVAH
	.extern	_DIVAL
	.extern	_DIVB
	.extern	_DIVQH
	.extern	_DIVQL
	.extern	_DIVR
	.extern	_LEDCTL
	.extern	_LEDPRE
	.extern	_LEDDATA0
	.extern	_LEDDATA1
	.extern	_LEDDATA2
	.extern	_LEDDATA3
	.extern	_LEDDATA4
	.extern	_LEDDATA5
	.extern	_LEDDATA6
	.extern	_LEDDATA7
	.extern	_LEDOMS1
	.extern	_LEDODS0
	.extern	_LEDOMS0
	.extern	_LEDODS1
	.extern	_VRECAL2
	.extern	_VRECAL3
	.extern	_LEDLUM
	.extern	_T4L
	.extern	_T4H
	.extern	_T4REL
	.extern	_T4REH
	.extern	_T4CTL
	.extern	_RC32KCAL
	.extern	_startup
	.extern	__sdcc_gsinit_startup
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.global	_init_mcu
	.global	_init_WDT
	.global	_main

	.global I0R1
	.global I0R7
	.global I0PSW
	.global I0PCH
	.global I1R1
	.global I1R7
	.global I1PSW
	.global I1PCH
	.global STK11
	.global STK10
	.global STK09
	.global STK08
	.global STK07
	.global STK06
	.global STK05
	.global STK04
	.global STK03
	.global STK02
	.global STK01
	.global STK00

sharebank .udata
I0R1		.res 1
I0R7		.res 1
I0PSW		.res 1
I0PCH		.res 1
I1R1		.res 1
I1R7		.res 1
I1PSW		.res 1
I1PCH		.res 1
STK11	.res 1
	.type STK11, 108
STK10	.res 1
	.type STK10, 108
STK09	.res 1
	.type STK09, 108
STK08	.res 1
	.type STK08, 108
STK07	.res 1
	.type STK07, 108
STK06	.res 1
	.type STK06, 108
STK05	.res 1
	.type STK05, 108
STK04	.res 1
	.type STK04, 108
STK03	.res 1
	.type STK03, 108
STK02	.res 1
	.type STK02, 108
STK01	.res 1
	.type STK01, 108
STK00	.res 1
	.type STK00, 108

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	.udata_ovr
;--------------------------------------------------------
; reset vector 
;--------------------------------------------------------
STARTUP	.code 0x0000
	NOP
	PAGESEL	init_imp
	JMP		init_imp

	.global	init_imp
INIT_IMP	.code
init_imp
	PAGESEL	_startup
	CALL	_startup
	PAGESEL	__sdcc_gsinit_startup
	CALL	__sdcc_gsinit_startup
	PAGESEL	_main
	JMP		_main


func._main	.code
;***
;  PostBlock Stats: dbName = M
;***
;entry:  _main	;Function start
; 2 exit points
;has an exit
;functions called:
;   _init_mcu
;   _init_WDT
;   _init_mcu
;   _init_WDT
;; Starting PostCode block
;	op : LABEL
;	op : FUNCTION
_main	;Function start
; 2 exit points
;	op : CALL
;	.line	59; "../main.c"	init_mcu();
	PAGESEL	_init_mcu
	CALL	_init_mcu
	PAGESEL	$
;	op : CALL
;	.line	60; "../main.c"	init_WDT();
	PAGESEL	_init_WDT
	CALL	_init_WDT
	PAGESEL	$
;	op : LABEL
;	op : GET_VALUE_AT_ADDRESS
;	op : !
;	op : =
_00006_DS_
;	.line	63; "../main.c"	P1LR3=!P1LR3;
	BANKSEL	_P1LR_bits
	JB	_P1LR_bits, 3
	JMP	_00011_DS_
	BANKSEL	_P1LR_bits
	CLR	_P1LR_bits, 3
	JMP	_00012_DS_
_00011_DS_
	BANKSEL	_P1LR_bits
	SET	_P1LR_bits, 3
;	op : INLINEASM
_00012_DS_
	NOP 
;	op : INLINEASM
	NOP 
;	op : =
;	.line	66; "../main.c"	SWDTEN=1;
	BANKSEL	_PCTL_bits
	SET	_PCTL_bits, 2
;	op : INLINEASM
	CWDT 
;	op : INLINEASM
	IDLE 
;	op : =
;	.line	69; "../main.c"	SWDTEN=0;
	BANKSEL	_PCTL_bits
	CLR	_PCTL_bits, 2
;	op : GOTO
	JMP	_00006_DS_
;	op : LABEL
;	op : ENDFUNCTION
	CRET	
; exit point of _main



func._init_WDT	.code
;***
;  PostBlock Stats: dbName = C
;***
;entry:  _init_WDT	;Function start
; 2 exit points
;has an exit
;; Starting PostCode block
;	op : LABEL
;	op : FUNCTION
_init_WDT	;Function start
; 2 exit points
;	op : =
;	.line	49; "../main.c"	WDTPS =0xFB;	  //看门狗定时器预分频1:65536
	MOV	R0,# 0xfb
	BANKSEL	_WDTPS
	MOV	_WDTPS, R0
;	op : =
;	.line	50; "../main.c"	PSA = 1;          //预分频器控制位，选择用于WDT
	BANKSEL	_OPTR_bits
	SET	_OPTR_bits, 3
;	op : =
;	.line	52; "../main.c"	PS0 = 0;          //1:1分频，18ms定时
	BANKSEL	_OPTR_bits
	CLR	_OPTR_bits, 0
;	op : =
;	.line	53; "../main.c"	PS1 = 0;
	BANKSEL	_OPTR_bits
	CLR	_OPTR_bits, 1
;	op : =
;	.line	54; "../main.c"	PS2 = 0;
	BANKSEL	_OPTR_bits
	CLR	_OPTR_bits, 2
;	op : LABEL
;	op : ENDFUNCTION
	CRET	
; exit point of _init_WDT



func._init_mcu	.code
;***
;  PostBlock Stats: dbName = C
;***
;entry:  _init_mcu	;Function start
; 2 exit points
;has an exit
;; Starting PostCode block
;	op : LABEL
;	op : FUNCTION
_init_mcu	;Function start
; 2 exit points
;	op : =
;	.line	25; "../main.c"	OSCCTL = 0x60;          //设置为16M
	MOV	R0,# 0x60
	BANKSEL	_OSCCTL
	MOV	_OSCCTL, R0
;	op : =
;	.line	27; "../main.c"	TR0 = 0x04;				//设置VPP P02端口为输入，P0其他I/O口为输出
	MOV	R0,# 0x04
	BANKSEL	_TR0
	MOV	_TR0, R0
;	op : =
;	.line	28; "../main.c"	TR1 = 0x00;				//设置P1端口为输出
	BANKSEL	_TR1
	CLR	_TR1
;	op : =
;	.line	29; "../main.c"	TR2 = 0x00;				//设置P2端口为输出
	BANKSEL	_TR2
	CLR	_TR2
;	op : =
;	.line	30; "../main.c"	TR3 = 0x00;				//设置P3端口为输出
	BANKSEL	_TR3
	CLR	_TR3
;	op : =
;	.line	31; "../main.c"	P0LR = 0;
	BANKSEL	_P0LR
	CLR	_P0LR
;	op : =
;	.line	32; "../main.c"	P1LR = 0;
	BANKSEL	_P1LR
	CLR	_P1LR
;	op : =
;	.line	33; "../main.c"	P2LR = 0;
	BANKSEL	_P2LR
	CLR	_P2LR
;	op : =
;	.line	34; "../main.c"	P3LR = 0;
	BANKSEL	_P3LR
	CLR	_P3LR
;	op : =
;	.line	35; "../main.c"	P0 = 0;
	BANKSEL	_P0
	CLR	_P0
;	op : =
;	.line	36; "../main.c"	P1 = 0;
	BANKSEL	_P1
	CLR	_P1
;	op : =
;	.line	37; "../main.c"	P2 = 0;
	BANKSEL	_P2
	CLR	_P2
;	op : =
;	.line	38; "../main.c"	P3 = 0;
	BANKSEL	_P3
	CLR	_P3
;	op : LABEL
;	op : ENDFUNCTION
	CRET	
; exit point of _init_mcu


;	code size estimation:
;	   38+   23 =    61 instructions (  168 byte)


	.end
