/*
 * W_BEE.h
 *
 *  Created on: 2016-4-19
 *      Author: Administrator
 */

#ifndef W_BEE_H_
#define W_BEE_H_

#define			BEE_BUFFER_MAX	16		// 数据的大小与单次操作量相关，这里int型，但也只能是4，8，12，16，除非不用写操作。
extern unsigned int BEE_READ_ONE(unsigned int);
extern void 			BEE_READ_FUN	(unsigned int ,unsigned char );//读多个数据到数据缓存区，建议最多量为1个页即16个数据，同时建议不跨页操作，与写函数对应
extern void 			BEE_WRITE_ONE	(unsigned int ,unsigned int );  // 过程封装
extern void 			BEE_WRITE_FUN	(unsigned int ,unsigned char ); //将缓存区数据写到对应位置，从第一个开始

extern unsigned int 	BEE_BUFFER[BEE_BUFFER_MAX];
extern unsigned int     BEE_READ_BUF;	//当单字读函数使用非全嵌汇编模式时，使用该值获取读取结果R7 R6构成的值

#endif /* W_BEE_H_ */
