/****************************************************************************************
 *
 * 文  件  名: main.c
 * 项  目  名: 14-IIC_Master
 * 版         本: v1.0
 * 日         期: 2016年05月16日 11时38分22秒
 * 作         者: Administrator
 * 程序说明：IIC通信例程
 * 			——主机模式
 * 适用芯片：KF8FXXXX系列——KF8F2156、KF8F3156、KF8F4156、KF8F3155、KF8F4155、KF8F4158
 * 			KF8TS25XX系列——KF8TS2508、KF8TS2510、KF8TS2514、KF8TS2516
 * 			KF8TS27XX系列——KF8TS2708、KF8TS2710、KF8TS2714、KF8TS2716
 * 			KF8VXXX系列——KF8V327、KF8V427、KF8V429
 ****************************************************************************************/
#include<KF8F4158.h>
#define uchar unsigned char
#define uint  unsigned int

#define		IIC_10BIT_Address		0			//0——7位地址模式，1——10位地址模式

#define	LED4	P2LR0

#define KEY3   P03
#define KEY4   P05

#define SCLIO	TR10
#define SDAIO	TR11

#define Band	16		//99-10K;49-20K;32-30K;24-40K;19-50K;16-60K;

uint	time=0;
uint    Freq=0;

uchar	Temp=0;
uchar	ADDR=0;
uchar	DATA=0;
uchar	DATAcnt=0;
uchar	ON=0;

void Delay_ms(uint ms_data)
{
	uint j=0;
	while(ms_data--)
	{
		j=63;
		while(j--)
		{
		}
	}
}

//中断函数0:0X04入口地址
void int_fun0() __interrupt (0)
{
	if(T0IF)
	{
		T0IF = 0;
		T0 = 0X9C;
	}
}

//中断函数1:0x14入口地址
void int_fun1() __interrupt (1)
{

}

//启动IIC
void Start_I2C(void)
{
	STARTEN=1;			//发送START。由硬件自动清零
	while(STARTEN);
	while(!SSCIIF);
	SSCIIF=0;
}

//停止IIC
void Stop_I2C()
{
	STOPEN=1;
	while(STOPEN);
	while(!SSCIIF);
	SSCIIF=0;
}

//发送数据，等待应答
void SendB(uchar c )
{
	uchar j=0;
	SSCIIF=0;
	SSCIBUFR=c;

	while(SSCIBUF);		//发送完成SSCIBUF清零

	while(!SSCIIF);

	while(SSCIACKSTA && j<100)		//判断是否收到应答
	{j++;}
}

//接收数据
uchar RcvB()
{
	uchar da=0;
	SSCIRCEN=1;
	while(SSCIRCEN)		//接收完毕，硬件自动清零
	{
		LED4=1;
	}
	LED4=0;

	while(!SSCIIF)
	{
		LED4=1;
	}
	LED4=0;
	SSCIIF=0;

	da=SSCIBUFR;

	return da;
}

//发送应答，0-应答;1-不应答
void ack(uchar a)
{
	if(a)SSCIACKDAT=1;//应答信号=1，不应答；应答信号=0，应答
	else SSCIACKDAT=0;
	SSCIACKEN=1;		//使能应答信号发送

	while(SSCIACKEN)	//发送完ACKDAT，硬件自动清零
	{
		LED4=1;
	}
	LED4=0;
	while(!SSCIIF)
	{
		LED4=1;
	}
	LED4=0;
	SSCIIF=0;
}

void init_mcu()
{
	OSCCTL = 0x70;           //32M
	_NOP();
	_NOP();

	//端口初始化
	TR0 = 0X2E;              //0000 1110
	TR1 = 0X03;              //0000 0000 P10为SCK，P11为SDA
	TR2 = 0X00;              //0000 0000
	TR3 = 0X00;				 //0000 0000

	P0LR = 0X38;             //0011 1000
	P1LR = 0X03;             //0000 0011
	P2LR = 0x00;             //0011 0001
	P3LR = 0X00;			 //0101 0000

	//I2C初始化
	SSCIEN=0;				//关闭I2C模块，初始化完之后再打开

	SSCICTL0=0X18;			//I2C主控模式
	SSCICTL1=0X00;
	SSCISTA=0X00;

	SSCIADD=79;	//主控模式下，该寄存器[Fosc/(BandRate*4)]-1决定了波特率
					//49-20K;32-30K;24-40K;19-50K;
	SSCIIF=0;				//清零I2C中断标志
	SSCIEN=1;

	//定时器初始化
    OPTR = 0X03;     //16分频，4us计数1，
    T0 = 0X06;      //1ms定时
    T0IE = 1;
    T0IF = 0;

    //中断初始化
    AIE = 1;		//打开总中断
}

void main()
{
	uchar i=0,j=0;
	uchar da=0;
	init_mcu();

	while(1)
	{
		LED4=0;
		if(!KEY3)
		{
			Delay_ms(5);
			if(!KEY3)
			{
				while(!KEY3);
				i++;
				if(i>7)i=1;
				switch(i)
				{
					case 1:da=0xE1;break;
					case 2:da=0xE2;break;
					case 3:da=0xE3;break;
					case 4:da=0xE4;break;
					case 5:da=0xE5;break;
					case 6:da=0xE6;break;
					case 7:da=0xE7;break;
					default:break;
				}

				Start_I2C();
#if	IIC_10BIT_Address						//发送10位地址
				SendB(0XF6);
				SendB(0X5A);
#else
				SendB(0X5A);				//发送7位地址
#endif
				SendB(da);
				SendB(0X61);
				SendB(0X62);
				SendB(0X63);
				SendB(0X64);
				SendB(0X65);
				SendB(0X66);
				Stop_I2C();
				Start_I2C();
#if	IIC_10BIT_Address						//发送10位地址
				SendB(0XF7);
				SendB(0X5A);
#else
				SendB(0X5B);				//发送7位地址
#endif

				da=RcvB();
				ack(0);

				da=RcvB();
				ack(0);

				da=RcvB();
				ack(0);

				da=RcvB();
				ack(0);

				da=RcvB();
				ack(0);

				da=RcvB();
				ack(0);

				da=RcvB();
				ack(0);

				da=RcvB();
				ack(1);

				Stop_I2C();
			}
		}

		if(!KEY4)
		{
			Delay_ms(5);
			if(!KEY4)
			{
				while(!KEY4);
				j++;
				if(j>2)j=1;
				if(j==1)
				{
					Start_I2C();
#if	IIC_10BIT_Address					//发送10位地址
					SendB(0XF6);
					SendB(0X5A);
#else
					SendB(0X5A);		//发送7位地址
#endif
					SendB(0X55);
					Stop_I2C();
				}
				if(j==2)
				{
					Start_I2C();
#if	IIC_10BIT_Address					//发送10位地址
					SendB(0XF7);
					SendB(0X5A);
#else
					SendB(0X5B);		//发送7位地址
#endif
					SendB(0XAA);
					Stop_I2C();
				}
			}
		}
	}
}
